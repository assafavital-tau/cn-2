//
//  serverAux.h
//  Communication_Networks
//
//  Created by Yossi Elman on 12/2/16.
//  Copyright © 2016 Yossi Elman. All rights reserved.
//

#ifndef serverAux_h
#define serverAux_h

#include <stdio.h>

#define WELCOME_MSG "Welcome! I am simple-mail-server! In Mother Russia i was a doctor"
#define FAILED_SEND "Failed to send: %s\n"
#define FAILED_RECIEVE "Failed to recieve: %s\n"
#define INVALID_NUM_ARG "Invalid number of arguments, Bye...\n"
#define INVALID_ARG "Invalid argument, Bye...\n"
#define INVALID_PORT "Invalid port value, Bye...\n"
#define FAILED_OPEN_SOCKET "Failed to open socket: %s\n"
#define FAILED_BIND "Failed to bind: %s\n"
#define FAILED_LISTEN "Failed to listen: %s\n"
#define FAILED_ACCEPT "Failed to accept: %s\n"
#define FAILED_AUTHENTICATE "Failed to authenticate, Bye..\n"
#define FAILED_ALLOCATE "Failed to allocate memory, Bye..\n"
#define FAILED_SELECT "Failed to Select: %s\n"
#define FAILED_CLOSE "Failed to close socket descriptor: %s\n"
#define OFFLINE_SUBJECT "Message received offline"
#define CHAT_SIGNAL "$chat"



#define NUM_OF_CLIENTS (20)
#define MAX_USERNAME (50)
#define MAX_PASSWORD (50)
#define BUFF_LEN (2048)
#define MAXMAILS (32000)
#define TOTAL_TO (20)
#define MAX_SUBJECT (100)
#define MAX_CONTENT (2000)
#define CMD_SIZE (25)


#define IGNORE_DELETED 0
#define COUNT_DELETED 1
#define DISPLAY_NEXT_MAIL 2

#define SHOW_INBOX 0 // comes with command_id = 0 (not relevant for me)
#define GET_MAIL 1
#define DELETE_MAIL 2
#define QUIT 3 // comes with command_id = 0 (not relevant for me)
#define COMPOSE 4
#define SHOW_ONLINE_USERS 5 //comes with command_id = 0 (not relevant for me)
#define CHAT_MSG 6 // comes with command_id = 0 (not relevant for me)
#define CMD_UNDEFINED 8 //default value

struct USER;


/**
 An ENUM that represents the current state of the USER
 ACCEPTED           - This user got accepted by server
 LOGGED_IN          - This user was granted access
 DISCONNECTED       - This user is not connected
 **/

typedef enum client_state
{
    ACCEPTED,
    LOGGED_IN,
    DISCONNECTED
} ClientState;


/**
 A structure for a MAIL at the platform
 char* from          - a char[MAX_USERNAME] that conteanis the username of the sender;
 char** to           - an array of char[MAX_USERNAME] array, which contains all the usernames which the mail
 was addressed to;
 char subject        - a char[100] which contains the subject of the mail;
 char text           - a char[2000] which contains the text content of the mail;
 int mail_id         - the index of this MAIL structure inside inbox.
 int deleted         - indicated whether this mail has benn deleted (==1 iff deleted).
 int numOfRecipients - the number of USER structures stored in the "to" array.
 
 **/

typedef struct MAIL
{
    char from[MAX_USERNAME];
    char to[TOTAL_TO][MAX_USERNAME];
    char subject[MAX_SUBJECT];
    char text[MAX_CONTENT];
    int mail_id[NUM_OF_CLIENTS];
    int deleted[TOTAL_TO];
    int numOfRecipients;
} Mail;


/**
 A structure for a USER at the platform
 char username  - a char[512] which contains the username of the USER;
 char password  - a char[512] which contains the password of the USER;
 int inboxSize  - indicates how many Mail structures in inbox are addressed to this User.
 int totalMails - indicates how many Mail  structures in total this user have received.
 int socket_fd - the socket fd this USER is using.
 ClientState current_state - an enum that represents the current state of this USER.
 int user_index - an int that mapps this USER to his position in database array
 int unsorted_index - an int that mapps this USER to his position in the unsorted_database array


 **/

typedef struct USER
{
    char username[MAX_USERNAME];
    char password[MAX_PASSWORD];
    int inboxSize;
    int totalMails;
    int socket_fd;
    ClientState current_state;
    int user_index;
    int unsorted_index;
} User;


/*******************************************************************************************************************/
/*******************************************************************************************************************/
/***********************************************FUNCTION DECLARATIONS***********************************************/
/*******************************************************************************************************************/
/*******************************************************************************************************************/

/**
 comperator for qsort.
 compares by the first string in a two-dimensional char**
 **/
int compare(const void *a, const void *b);

/**
 parsing the given users file into an array {[username],[password]}
 and sorting it lexicographically by username.
 the result will be stored in database addrese given.
 also, stores the total number of users on the database on the given argument and the unsorted one.
 **/
void setup(User* database, FILE* users, User* database_unsorted);

/**
 searches the username inside the database and returnes the index it is located.
 if the target isn't found - returns -1.
 **/

int binary_search(User * database, int size, char *target);

/**
 a function that calculates the number of lines in the txt file given by the user.
 returns the number of lines on the file which is also the number of users.
 **/

int getNumOfUsers(FILE* users);
//int sendBuff(int socket, char* buff,int len);


/**
 an auxilary function to construct a TO string which consists the usernames seperated by ",".
 the result will be stored in the res address given as an argument.
 **/

void makeToSting(char* res, char to[TOTAL_TO][MAX_USERNAME], int size);

/**
 Parse the buff to username and password and checks if there is a matching pair (by username) inside database.
**/
int authenticate(int fd_socket, int* index, char* buff,User database[][NUM_OF_CLIENTS], int numOfUsers, int socket,User database_unsorted[][NUM_OF_CLIENTS]);
/**
 Based on the command passed as an argument, serves the client’s wishes.
**/
void handleCommand(char command, int cmd_id, int user_index, int fd_socket,
                   User (*database)[NUM_OF_CLIENTS], int numOfUsers, Mail** inbox ,int* totalMails, ClientState state);

/**
 takes a string in the format - "TO: User1, User2, ... User_n" and converts it to an array of char[].
 the result array will be stored in the given address.
 **/

void parseToString(char* buff, char to [TOTAL_TO][MAX_USERNAME], int numOfRecipients, int numOfUsers );

/**
 Clears the char* given from any content and sets all the cells to 0.
 **/
void clearBuff(char* buff, int size);
/**
 composes a new Mail structure, from the arguments given and stores it at the inbox of the given pUser,
 at index [totalMails].
 **/
void composeNewMail(Mail* newMail, User* pUser, User* currentUser,char to[TOTAL_TO][MAX_USERNAME], char* subject,
                    char* text, int cmd_id, int index );

/**
 a getter function to get the index of the next Mail that was sent to username
 a getter function to get the index of the next Mail that was sent to username
 Operates according to the following flags:
 IGNORE_DELETED - will detect a mail of this user only if it wasn’t deleted
 COUNT_DELETED - will detect all mail that were sent to this user
 DISPLAY_NEXT_MAIL - if we need to display the next mail, it is crucial to know whether the next mail is deleted or not
 (so we won’t display a mail that has been deleted, or get in an infinite loop)
 
 **/
int getNextMail (Mail* inbox, char username[MAX_USERNAME], int current_index, int ignoreDeleted);


/**
 deletes the next mail in inbox, given current index to start checking from.
 practically changes the first char that username to null, then strcmp will fail connecting this mail to that username.
 **/

void deleteNextMail (Mail* inbox, char username[MAX_USERNAME], int current_index);


/*
 The same function as seen in recitation
 */
int recvall(int sock, void *buf, int *len);

/*
 The same function as seen in recitation
 */

int sendall(int sock, const void *buf, int len);

/**
 An envelope function that first gets the length of the buffer from client, and then calls the function we've seen in the recitation, with the relevant length.
 Returns 0 on success and in case of an error returns -1.
 
 **/
int get_message(int sock , char* message);

/**
 An envelope function that first sends the length of the buffer to client, and then calls the function we've seen in the recitation.
 Returns 0 on success and in case of an error returns -1.
 **/
int send_message(int sock, const void *buf, int len);


/**
 Given a socket_fd, this function returns the user index at the database array of the users using this socket.
 **/
int getUserIndexFromSocket(int socket, User* database, int numOfUsers);


/**
 A getter for the socket fd of the USER given his USER struct.
 **/

int getSocketFromUser(char* target, User* database);

/**
 A handler for show_online_users cmd
 **/

void showOnlineUsers(int fd_socket,User (*database)[NUM_OF_CLIENTS]);

/**
A handler for show_inbox cmd
 **/

void showInbox(int user_index, int fd_socket, User (*database)[NUM_OF_CLIENTS], Mail** inbox, int* totalMails);

/**
 A handler for get_mail cmd
 **/
void getMailFunc(int cmd_id, int user_index,int fd_socket,User (*database)[NUM_OF_CLIENTS],Mail** inbox, int* totalMails);

/**
 A handler for delete_mail cmd
 **/
void deleteMail(int cmd_id, int user_index,User (*database)[NUM_OF_CLIENTS],Mail** inbox);

/**
 A handler for compose cmd
 **/
void compose(int cmd_id, int user_index,int fd_socket,User (*database)[NUM_OF_CLIENTS],
             int numOfUsers, Mail** inbox, int* totalMails);

/**
 A handler for (chat) MSG cmd
 **/
void handleChatMsg(int user_index,int fd_socket,User (*database)[NUM_OF_CLIENTS],int numOfUsers,
                   Mail** inbox, int* totalMails);


/**
This function return 1 iff the given char is the username of a connected USER - else returns 0.
 **/

int checkIfOnline(char* target, User* database);

/**
 This function handles the disconnection of a USER by setting the socked_fd and STATE fields at the database and the unsorted_databade arrays.
 **/

void disconnect(User* client);

/**
 This function handles the USER given his state
 **/
int handleClient(User* client);

/**
An aux function to given the sorted_index of a USER return his index in the unsorted_database
 **/
int convertSortedToUnsorted(int sorted_index, User* database, User* database_unsorted);



#endif /* serverAux_h */
