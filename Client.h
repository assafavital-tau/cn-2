/*
 * client.h
 *
 *  Created on: Nov 14, 2016
 *      Author: assaf
 */

#ifndef CLIENT_H_
#define CLIENT_H_

#include "ClientUtils.h"

#define DEF_CLA std::string hostname = "localhost"; \
		u_short port_no = 6423;

#define EXECUTE(C,M) if(command == C) {client->M(); continue;}
#define EXECUTE_INT_PARAM(C,M) if(command.compare(0, C.length(), C) == 0) { \
		int_param = STR2INT(command.substr(C.length())); \
		client->M(int_param); \
		continue; }
#define EXECUTE_STR_PARAM(C,M) if(command.compare(0, C.length(), C) == 0) { \
		str_param = command.substr(C.length()); \
		client->M(str_param); \
		continue; }
#define INIT_PARAM_COMM std::string get_mail = "get_mail "; \
		std::string delete_mail = "delete_mail "; \
		std::string msg = "msg ";
#define FD_RESET FD_ZERO(&s); FD_SET(0, &s); FD_SET(socket_fd, &s);

class TCPClient
{
private:
	int socket_fd;
	int port_no;
	std::string address;
	struct sockaddr_in server;

	std::queue<Chat> chatQ;

private:
	bool authenticate();
	void sendMsg(Message msg);

public:
	TCPClient(const std::string& hostname, u_short port);

	bool connectToServer();

	void showInbox();
	void getMail(int id);
	void deleteMail(int id);
	void quit();
	void compose();
	void showOnlineUsers();
	void chat(const std::string& msg);

	void listenToChat();
};

TCPClient* initFromCLA(int argc, char* argv[]);
int main(int argc, char* argv[]);

#endif /* CLIENT_H_ */
