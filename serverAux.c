//
//  serverAux.c
//  Communication_Networks
//
//  Created by Yossi Elman on 12/2/16.
//  Copyright © 2016 Yossi Elman. All rights reserved.
//

#include "serverAux.h"
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>


void handleChatMsg(int user_index,int fd_socket,User (*database)[NUM_OF_CLIENTS],int numOfUsers,
                   Mail** inbox, int* totalMails)
{
    User* currentUser = &(*database)[user_index];
    int index;
    char buff[BUFF_LEN];
    int isOnline;
    int sizeOfBuff;
    int user_socket;
    clearBuff(buff, BUFF_LEN);
    char recipient[MAX_USERNAME];
    char to[TOTAL_TO][MAX_USERNAME];
    char text[MAX_CONTENT];
    User* pUser;
    Mail newMail = { 0, 0, 0,0,0,0,0};
    
    if ((get_message(fd_socket, recipient)) == -1){
        printf(FAILED_RECIEVE, strerror(errno));
        return;
    }
    if (get_message(fd_socket, text)== -1){
        printf(FAILED_RECIEVE, strerror(errno));
        return;
    }
    
    user_socket = getSocketFromUser(recipient, *database);
    
    isOnline = checkIfOnline(recipient, *database);
    
    if (isOnline)
    {
        //create the chat msg
        sizeOfBuff = sprintf(buff, "%s%s: %s",CHAT_SIGNAL,currentUser->username , text);
        //sending to "to"
        if (send_message(user_socket, buff, sizeOfBuff) == -1){
            printf(FAILED_SEND, strerror(errno));
            return;
        }
    }
    else
    {
        index = binary_search(*database, numOfUsers, recipient);
        pUser = &((*database)[index]);
        parseToString(recipient, to, 1, numOfUsers);
        composeNewMail(&newMail, pUser, currentUser, to, "Message received offline", text, 1, index);
        pUser->inboxSize++;
        pUser->totalMails++;
        memcpy(((*inbox)+(*totalMails)), &newMail, sizeof(newMail));
        (*totalMails)++;
    }
}



//if(command == SHOW_INBOX ){
void showOnlineUsers(int fd_socket,User (*database)[NUM_OF_CLIENTS])
{
    int i;
    int onlineClients =0;
    char signal[3];
    int len;
    int isOnline[NUM_OF_CLIENTS];
    for (i=0; i<NUM_OF_CLIENTS; i++)
    {
        if ((*database)[i].current_state != DISCONNECTED)
        {
            onlineClients++;
            isOnline[i] = 1;
        }
    }
    //signal to client how many online users to expect
    len = (int) sprintf(signal, "%d", onlineClients);
    if (send_message(fd_socket, signal, len) == -1)
    {
        printf(FAILED_SEND, strerror(errno));
        return;
    }
    for (i=0; i<NUM_OF_CLIENTS; i++)
    {
        if (isOnline[i] == 1)
        {
            if ( send_message(fd_socket, (*database)[i].username ,(int)strlen((*database)[i].username)) == -1)
            {
                printf(FAILED_SEND, strerror(errno));
                return;
            }
        }
    }
}


/*****************************************SHOW_INBOX**********************************************************/


void showInbox(int user_index, int fd_socket, User (*database)[NUM_OF_CLIENTS], Mail** inbox, int* totalMails)
{
    User* currentUser = &(*database)[user_index];
    Mail getMail = { 0, 0, 0,0,0,0,0};
    int i, actual_mail, sizeOfBuff;
    char buff[BUFF_LEN];
    char signal[6];
    int len;
    //notify the client how many mails to expect
    len = (int) sprintf(signal, "%d", currentUser->inboxSize);
    //signal to client how many mails to expect
    if (send_message(fd_socket, signal, len) == -1)
    {
        printf(FAILED_SEND, strerror(errno));
        return;
    }
    actual_mail =0;
    i=0;
    while (actual_mail< (currentUser->inboxSize)){
        i = getNextMail(*inbox, currentUser->username, i, IGNORE_DELETED);//start with i=0,  ignore deleted mails
        getMail = (*inbox)[i];
        sizeOfBuff = sprintf(buff, "%d %s \"%s\"",getMail.mail_id[user_index],getMail.from, getMail.subject);
        if (send_message(fd_socket, buff, sizeOfBuff) == -1){
            printf(FAILED_SEND, strerror(errno));
            return;
        }
        actual_mail++;
        i++;
    }
}

/*****************************************GET_MAIL**********************************************************/

//if( command == GET_MAIL){
void getMailFunc(int cmd_id, int user_index,int fd_socket,User (*database)[NUM_OF_CLIENTS],Mail** inbox, int* totalMails)
{
    User* currentUser = &(*database)[user_index];
    char valid_cmd_id[2];
    Mail getMail = {0,0,0,0,0,0,0};
    char buff[BUFF_LEN];
    int i,j;
    //mail_id is too big
    if ((cmd_id > currentUser->totalMails) || (cmd_id == 0)){
        strcpy(valid_cmd_id, "0");
        if (send_message(fd_socket, valid_cmd_id, (int) strlen(valid_cmd_id)) == -1)
        {
            printf(FAILED_SEND, strerror(errno));
        }
        return;
    }
    //client knows to expect 4 packets - one for each line
    i=0;
    for (j=0;j<cmd_id-1;j++){//get the mail at index cmd_id, notice that we are not ignoring deleted mails.
        i = getNextMail(*inbox, currentUser->username, i,COUNT_DELETED);
        i++;
    }
    i = getNextMail(*inbox, currentUser->username, i,DISPLAY_NEXT_MAIL);
    //the user requested for a mail that has been deleted
    if (i<0){
        strcpy(valid_cmd_id, "0");
        if (send_message(fd_socket, valid_cmd_id, (int) strlen(valid_cmd_id)) == -1)
        {
            printf(FAILED_SEND, strerror(errno));
        }
        return;
    }
    
    else{// valid cmd_id
        strcpy(valid_cmd_id, "1");
        if (send_message(fd_socket, valid_cmd_id, (int) strlen(valid_cmd_id)) == -1)
        {
            printf(FAILED_SEND, strerror(errno));
            return;
        }
    }
    getMail = (*inbox)[i];
    //send FROM:
    sprintf(buff, "%s%s", "From: ", getMail.from );
    if ( send_message(fd_socket, buff , (int) strlen(buff)) == -1){
        printf(FAILED_SEND, strerror(errno));
        return;
    }
    
    //send TO:
    makeToSting(buff, getMail.to, getMail.numOfRecipients);
    if ( send_message(fd_socket, buff ,(int) strlen(buff)) == -1){
        printf(FAILED_SEND, strerror(errno));
        return;
    }
    clearBuff(buff, BUFF_LEN);
    
    //send SUBJECT:
    sprintf(buff, "%s%s", "Subject: ", getMail.subject );
    if ( send_message(fd_socket, buff , (int) strlen(buff)) == -1){
        printf(FAILED_SEND, strerror(errno));
        return;
    }
    clearBuff(buff, BUFF_LEN);
    //send TEXT:
    sprintf(buff, "%s%s", "Text: ", getMail.text );
    if ( send_message(fd_socket, buff , (int) strlen(buff)) == -1){
        printf(FAILED_SEND, strerror(errno));
        return;
    }
    clearBuff(buff, BUFF_LEN);
    
}



/*****************************************DELETE_MAIL**********************************************************/

void deleteMail(int cmd_id, int user_index,User (*database)[NUM_OF_CLIENTS],Mail** inbox)
{
    User* currentUser = &(*database)[user_index];
    int i,j;
    if (cmd_id <= currentUser->totalMails && cmd_id!=0)
    {//act only if cmd_id is in range
        i=0;
        for (j=0;j<cmd_id-1;j++)
        {//get to the mail at index cmd_id-1, notice that we are not ignoring deleted mails.
            i = getNextMail(*inbox, currentUser->username, i,COUNT_DELETED);
            i++;
        }
        deleteNextMail(*inbox, currentUser->username, i);
        //(*database)[0].inboxSize-=-1;
        currentUser->inboxSize -=1;
    }
}

/*****************************************COMPOSE**********************************************************/
void compose(int cmd_id, int user_index,int fd_socket,User (*database)[NUM_OF_CLIENTS],
             int numOfUsers, Mail** inbox, int* totalMails)
{
    User* currentUser = &(*database)[user_index];
    int j,index;
    char buff[BUFF_LEN];
    clearBuff(buff, BUFF_LEN);
    char to[TOTAL_TO][MAX_USERNAME];
    char subject[MAX_SUBJECT];
    char text[MAX_CONTENT];
    User* pUser;
    Mail newMail = { 0, 0, 0,0,0,0,0};
    
    //get TO:
    if (get_message(fd_socket, buff) == -1){
        printf(FAILED_RECIEVE, strerror(errno));
        return;
    }
    parseToString(buff, to, cmd_id, numOfUsers);
    
    //get SUBJECT:
    if ((get_message(fd_socket, subject)) == -1){
        printf(FAILED_RECIEVE, strerror(errno));
        return;
    }
    //get TEXT:
    if (get_message(fd_socket, text)== -1){
        printf(FAILED_RECIEVE, strerror(errno));
        return;
    }
    for (j=0; j<cmd_id; j++){
        index = binary_search(*database, numOfUsers, to[j]);
        pUser = &((*database)[index]);
        composeNewMail(&newMail, pUser, currentUser, to, subject, text, cmd_id, index);
        pUser->inboxSize++;
        pUser->totalMails++;
        memcpy(((*inbox)+(*totalMails)), &newMail, sizeof(newMail));
    }
    (*totalMails)++;
}



int authenticate(int fd_socket, int* index, char* buff,User database[][NUM_OF_CLIENTS], int numOfUsers, int socket,User database_unsorted[][NUM_OF_CLIENTS]){
    
    
    char username[MAX_USERNAME];
    char password[MAX_PASSWORD];
    char login;
    int unsroted_index;
    int stat;
    stat =get_message(fd_socket, buff);
    if (stat == -1){
        printf(FAILED_RECIEVE, strerror(errno));
        return -1;
    }
    sscanf(buff, "%s %s", username, password);
    (*index) = binary_search((*database), numOfUsers, username);
    
    //login = 1 <===> username and password entered by user matches the details in the file.
    if ( (*index >= 0) && (!strcmp(password, (*database)[*index].password))){
        login = 1;
        if (send(fd_socket ,&login, 1, 0)<0){
            printf(FAILED_SEND, strerror(errno));
            return -1;
        }
        (*database)[*index].socket_fd = socket;
        (*database)[*index].current_state= LOGGED_IN;
        unsroted_index = (*database)[*index].unsorted_index;
        (*database_unsorted)[unsroted_index].current_state= LOGGED_IN;
        (*database)[*index].user_index= *index;
        return 0;
    }
    
    else{// login failed
        login = 0;
        if(send(fd_socket, &login, 1, 0)<0){
            printf(FAILED_SEND, strerror(errno));
            return -1;
        }
        return -1;
    }
}

/**
 composes a new Mail structure, from the arguments given and stores it at the inbox of the given pUser,
 at index [totalMails].
 **/

void composeNewMail(Mail* newMail, User* pUser, User* currentUser, char to[TOTAL_TO][MAX_USERNAME], char* subject,
                    char* text, int cmd_id, int index){
    
    int k;
    strcpy(newMail->from, currentUser->username);
    for (k=0; k<cmd_id; k++){
        strcpy(newMail->to[k], to[k]);
        newMail->deleted[k] = 0;
    }
    strcpy(newMail->subject, subject);
    strcpy(newMail->text, text);
    newMail->mail_id[index] = ((pUser->totalMails)+1);
    newMail->numOfRecipients = cmd_id;
    
}

int getNextMail (Mail* inbox, char username[MAX_USERNAME], int current_index, int flag){
    int k = 0;
    Mail* currentMail;
    if (flag == IGNORE_DELETED){
        while (1){ // assuming that a Mail addressed to username exists for sure in inbox.
            currentMail = &(inbox[current_index]);
            for (k=0; k<currentMail->numOfRecipients; k++){
                if (!currentMail->deleted[k]){
                    if (!strcmp(currentMail->to[k], username)){
                        return current_index;
                    }
                }
            }
            current_index++;
        }
    }
    else if (flag == COUNT_DELETED){
        while (1){ // assuming that a Mail addressed to username exists for sure in inbox.
            currentMail = &(inbox[current_index]);
            for (k=0; k<currentMail->numOfRecipients; k++){
                if (!strcmp(currentMail->to[k], username)){
                    return current_index;
                }
            }
            current_index++;
        }
    }
    
    else {// flag == DISPLAY_NEXT_MAIL
        while (1){ // assuming that a Mail addressed to username exists for sure in inbox.
            currentMail = &(inbox[current_index]);
            for (k=0; k<currentMail->numOfRecipients; k++){
                if (!strcmp(currentMail->to[k], username)){
                    if (!currentMail->deleted[k]){
                        return current_index;
                    }
                    else{
                        return -1;
                    }
                }
            }
            current_index++;
        }
    }
}


void deleteNextMail (Mail* inbox, char username[MAX_USERNAME], int current_index){
    int user;
    Mail* currentMail;
    while (1){ // assuming that a Mail addressed to username exists for sure in inbox.
        currentMail = &(inbox[current_index]);
        for (user=0; user<currentMail->numOfRecipients; user++){
            if (!strcmp(currentMail->to[user], username)){
                currentMail->deleted[user]=1; // practically deletes this user from the recipients list.
                return;
            }
        }
        (current_index)++;
    }
}


void parseToString(char* buff, char to [TOTAL_TO][MAX_USERNAME], int cmd_id, int numOfUsers){
    if (cmd_id == 1){
        strcpy(to[0], buff);
        return;
    }
    char* username;
    int i=0;
    username = strtok (buff,",");
    while (username != NULL) {
        strcpy(to[i++], username);
        username= strtok (NULL, ",");
    }
    //printf("end of parse to: %s\n" buff);
}

void makeToSting(char* res, char to[TOTAL_TO][MAX_USERNAME], int size){
    int i;
    int curr = 0;
    strcpy(res, "To: ");
    curr += 4;
    if (size == 1){
        strcat(res, to[0]);
        return;
    }
    char temp[MAX_USERNAME+2];
    for (i=0;i<size-1;i++){
        strcpy(temp, to[i]);
        strcat(temp, ",");
        strcpy((res+curr), temp);
        curr += ((int) strlen(to[i])+1);
    }
    //no comma for the last one
    strcpy((res+curr), to[i]);
}



int getNumOfUsers(FILE* users){
    int ch, result = 0;
    do
    {
        ch = fgetc(users);
        if(ch == '\n')
            result++;
    } while (ch != EOF);
    
    // last line doesn't end with a new line!
    // but there has to be a line at least before the last line
    if(ch != '\n' && result != 0)
        result++;
    return result;
}

int binary_search(User* database, int size, char* target){
    int bottom= 0;
    int mid;
    int top = size - 1;
    while(bottom <= top){
        mid = (int) ((bottom + top)/2);
        if (strcmp(database[mid].username, target) == 0){
            return mid;
        } else if (strcmp(database[mid].username, target) > 0){
            top    = mid - 1;
        } else if (strcmp(database[mid].username, target) < 0){
            bottom = mid + 1;
        }
    }
    return -1;
}

void setup(User* database, FILE* users, User* database_unsorted){
    int i=0;
    int j;
    char line[MAX_USERNAME+MAX_PASSWORD+2];
    char username[MAX_USERNAME];
    char password[MAX_PASSWORD];
    while(fgets(line, sizeof(line), users)){
        sscanf(line, "%s\t%s", username, password);
        strcpy(database[i].username, username);
        strcpy(database[i].password, password);
        database[i].inboxSize = 0;
        database[i].totalMails = 0;
        i++;
    }
    memcpy(database_unsorted, database, NUM_OF_CLIENTS*sizeof(*database));
    qsort(database, i, sizeof(*database), compare);
    for (j=0; j<i; j++)
    {
        database[j].unsorted_index = convertSortedToUnsorted(j, database, database_unsorted);
    }
}


int compare(const void *a, const void *b)  {
    const User *ia = (const User *)a;
    const User *ib = (const User *)b;
    return strcmp((*ia).username, (*ib).username);
}

void clearBuff(char* buff, int size){
    int i;
    for(i=0;i<size; i++){
        (buff[i]) = 0;
    }
}

int recvall(int sock, void *buf, int *len) {
    int total = 0;        // how many bytes we've sent
    int bytesleft = *len; // how many we have left to send
    int n = 0;
    while(total < *len) {
        n = (int)recv(sock, (char *)buf+total, bytesleft, 0);
        if (n == -1) {
            break;
        }
        if (n == 0) {
            break;
        }
        total += n;
        bytesleft -= n;
    }
    *len = total; // return number actually sent here
    if (n == -1)
        return -1;
    if (n==0) // user disconnected
        return -2;
    return 0;
}
int sendall(int sock, const void *buf, int len) {
    int total = 0;        // how many bytes we've sent
    int bytesleft = len; // how many we have left to send
    int n = 0;
    while(total < len) {
        n = (int) send(sock, (char *)buf+total, bytesleft, 0);
        if (n == -1) {
            break;
        }
        total += n;
        bytesleft -= n;
    }
    return n == -1 ? -1:0; //-1 on failure, 0 on success
}


int send_message(int sock, const void *buf, int len) {
    // send the message (buf) size
    int size = 4;
    int buf_size = htonl(len);
    if (sendall(sock, &buf_size, size) == -1) {
        return -1; // failure
    }
    
    // send the message itself
    return sendall(sock, buf, len); //-1 on failure, 0 on success
}


int get_message(int sock , char* message) {
    
    // receive the size of the message to receive
    clearBuff(message, BUFF_LEN);
    int input;
    int size = 4;
    int status;
    status = recvall(sock, &input, &size);
    if (status == -1) {
        return -1;
    }
    if (status == -2)//user disconnected
        return -2;
    input = ntohl(input);
    // receive the message
    if (recvall(sock, message, &input) == -1) {
        return -1;
    }
    return 0;
}

int getUserIndexFromSocket(int socket, User* database, int numOfUsers)
{
    int i;
    for (i=0; i<numOfUsers; i++)
    {
        if (database[i].socket_fd == socket)
        {
            return i;
        }
        
    }
    return -1;
}

int getSocketFromUser(char* target, User* database)
{
    int i;
    for (i=0; i<NUM_OF_CLIENTS; i++)
    {
        if (strcmp(database[i].username, target) == 0){
            return database[i].socket_fd;
        }
    }
    return -1;
}

int checkIfOnline(char* target, User* database)
{
    int i;
    for (i=0; i<NUM_OF_CLIENTS; i++)
    {
        if (database[i].current_state != DISCONNECTED)
        {
            if (strcmp(database[i].username, target ) == 0)
                return 1;
        }
    }
    return 0;
}

int convertSortedToUnsorted(int sorted_index, User* database, User* database_unsorted )
{
    int i;
    for (i=0; i<NUM_OF_CLIENTS; i++)
    {
        if (strcmp(database[sorted_index].username, database_unsorted[i].username) == 0)
            return i;
    }
    //should not get here.
    return -1;
}




