#include "ClientUtils.h"

int Message::numOfRecipients()
{
	return 1 + std::count(recipients.begin(), recipients.end(), ',');
}

std::string getCmdBuff(Command c, int param)
{
	char cmd[6];
	cmd[0] = c;
	cmd[1] = ' ';
	memcpy((cmd+2), &param, sizeof(int));
	return std::string(cmd, 6);
}

bool recvBuff(int socket, char* buff)
{
	// Receive length
	int length;
	int tmp;
	size_t bytes_recvd = 0;
	while(bytes_recvd < sizeof(int))
	{
		tmp = recv(socket, (char*)(&length) + bytes_recvd, sizeof(int) - bytes_recvd, 0);
		if(tmp < 0)
		{
			PRT_ERR("Receive failed.")
			return false;
		}
		else
			bytes_recvd += tmp;
	}

//	PRT_DEBUG("before ntohl: " << length)

//	recv(socket, &length, sizeof(int), 0);

	return recvBuff(socket, buff, ntohl(length));
}

bool recvBuff(int socket, char* buff, int length)
{
//	PRT_DEBUG("about to receieve a buffer of length " << length)
    int bytes_recvd = 0, tmp;
    while(bytes_recvd < length)
    {
    	tmp = recv(socket, buff + bytes_recvd, length - bytes_recvd, 0);
    	if(tmp < 0)
    	{
    		PRT_ERR("Receive failed.")
    		return false;
    	}
    	else
    		bytes_recvd += tmp;
    }

    buff[length] = 0;
//    PRT_DEBUG("Ingoing: " << std::string(buff))
    return true;
}

bool sendBuff(int socket, std::string buff)
{
	// Send buffer length
	bool length_sent = sendLengthBuff(socket, buff.size());
	if(!length_sent) return false;

	// Send buffer itself
    const char *buffer = buff.c_str();
    int total_bytes = buff.size();

    int bytes_sent = 0, tmp;
    while(bytes_sent < total_bytes)
    {
    	tmp = send(socket, buffer + bytes_sent, total_bytes - bytes_sent, 0);
    	if(tmp < 0)
    	{
    		PRT_ERR("Send failed.")
    		return false;
    	}
    	else
    		bytes_sent += tmp;
    }

//    PRT_DEBUG("Outgoing: " << buff)
    return true;
}

bool sendLengthBuff(int socket, int length)
{
	length = htonl(length);
	char *len_buff = (char*)(&length);
	int tmp;
	size_t bytes_sent = 0;
	while(bytes_sent < sizeof(int))
	{
		tmp = send(socket, len_buff + bytes_sent, sizeof(int) - bytes_sent, 0);
		if(tmp < 0)
		{
			PRT_ERR("Send failed.")
			return false;
		}
		else
			bytes_sent += tmp;
	}

	return true;
}

void genChat(Chat* c, const std::string& buff)
{
	std::string new_chat = buff.substr(CHT_SIG.length());

	c->from = new_chat.substr(0, new_chat.find(':'));
	c->text = new_chat.substr(2 + new_chat.find(':'));
}
