#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <string>
#include <algorithm>
#include <locale>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <sys/select.h>
#include <queue>

#define PRT_ERR(M) std::cout << "\033[31;1m" << M << "\033[39;0m" << std::endl;
#define PRT_SUCC(M) std::cout << "\033[32;1m" << M << "\033[39;0m" << std::endl;
//#define PRT_DEBUG(M) std::cout << "\033[34;1m [DEBUG] \033[34;22m" << M << "\033[39;0m" << std::endl;
#define PRT_CHAT(C) std::cout << "\033[39;1mNew message from " << C.from << ": \033[39;0m" << C.text << std::endl;

#define MAX_BUFFER_SIZE 4096
#define NO_IA (in_addr_t)(-1)

#define STR2INT(S) (int)strtol(S.c_str(), NULL, 10)

#define CHT_SIG std::string("$chat")
#define IS_CHAT(M) M.find(CHT_SIG) != std::string::npos

enum Command
{
	SHOW_INBOX,
	GET_MAIL,
	DELETE_MAIL,
	QUIT,
	COMPOSE,
	SHOW_ONLINE_USERS,
	MSG
};

struct Chat
{
	std::string from;
	std::string text;
};

struct Message
{
	std::string recipients;
	std::string subject;
	std::string text;

	int numOfRecipients();
};

std::string getCmdBuff(Command c, int param);
bool recvBuff(int socket, char* buff);
bool recvBuff(int socket, char* buff, int length);
bool sendBuff(int socket, std::string buff);
bool sendLengthBuff(int socket, int length);
void genChat(Chat* c, const std::string& buff);
