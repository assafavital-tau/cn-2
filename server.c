
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include "serverAux.h"
#include "server.h"

/*** GLOBAL VARS ***/
FILE* users;
int numOfUsers;
int totalMails=0;
User database[NUM_OF_CLIENTS]= {{0},{0}};
User database_unsorted[NUM_OF_CLIENTS]= {{0},{0}};
Mail* inbox;


void disconnect(User* client)
{
    int unsorted_index;
    database[client->user_index].socket_fd = -1;
    database[client->user_index].current_state = DISCONNECTED;
    unsorted_index = database[client->user_index].unsorted_index;
    database_unsorted[unsorted_index].current_state = DISCONNECTED;
}

int handleClient(User* client)
{
    char buffer[BUFF_LEN];
    int cmd_id;
    int cmd;
    int j;
    int stat;
    char quit[2];
    // Authentication

    
    if((*client).current_state == ACCEPTED)
    {
        stat = authenticate((*client).socket_fd, &(client->user_index), buffer, &database, numOfUsers,(*client).socket_fd, &database_unsorted);
        if(stat == 0)
        {
            return 0;
        }
        else
        {
            disconnect(client);
            printf(FAILED_AUTHENTICATE);
            return -1;
        }
    }
    
    // Logged in
    if((*client).current_state == LOGGED_IN)
    {
        if(get_message((*client).socket_fd, buffer) == -1)
        {
            printf(FAILED_RECIEVE, strerror(errno));
            free(inbox);
        }
        else
        {
            cmd = buffer[0];
            cmd_id = buffer[5];
            for (j=1; j<4; j++){
                cmd_id <<= 8;
                cmd_id += (unsigned char) buffer[5-j];
            }
            
            
            if(cmd == SHOW_INBOX)
                showInbox( client->user_index, (*client).socket_fd,&database, &inbox, &totalMails);
            if(cmd == GET_MAIL)
                getMailFunc(cmd_id, client->user_index, (*client).socket_fd, &database, &inbox, &totalMails);
            if(cmd == DELETE_MAIL)
                deleteMail(cmd_id, client->user_index,  &database , &inbox);
            if(cmd == COMPOSE)
                compose(cmd_id, client->user_index, (*client).socket_fd, &database, numOfUsers, &inbox, &totalMails);
            if(cmd == QUIT)
            {
                //notify client he can quit
                strcpy(quit,"$");
                if (send_message((*client).socket_fd ,quit, 1)<0){
                    printf(FAILED_SEND, strerror(errno));
                    return -1;
                }
                disconnect(client);
                return -1;
            }
            if(cmd == SHOW_ONLINE_USERS)
                showOnlineUsers((*client).socket_fd, &database_unsorted);
            if(cmd == CHAT_MSG)
            {
                handleChatMsg(client->user_index, (*client).socket_fd, &database, numOfUsers, &inbox, &totalMails);
            }
            
        }
        return 0;
    }
    return 0;
}


int main(int argc, const char * argv[]) {
    
    /*******************************************************************************************************************/
    /*******************************************************************************************************************/
    /*******************************************ARGUMENTS CHECK AND DECLARATIONS****************************************/
    /*******************************************************************************************************************/
    /*******************************************************************************************************************/
    if (argc<2 || argc >3){
        printf("%s\n",INVALID_NUM_ARG );
        return -1;
    }
    users = fopen(argv[1], "r");
    if (!users){
        printf("%s\n", INVALID_ARG);
        return -1;
    }
    int fd_listen_socket,fd_socket, max_sd, sd ,status, user_index = 0, i=0;
    int client_socket[NUM_OF_CLIENTS];
    u_short port;
    socklen_t addr_size;
    struct sockaddr_in my_addr;
    struct sockaddr_in their_addr;
    fd_set readSet;
    User currentUser = {{0},{0},0,0,0,0};
    if (argc == 3){
        if (atoi(argv[2]) >= 0 && atoi(argv[2])<= 65535)//checking the given port is in the correct range
            port =  (u_short) atoi(argv[2]);
        else{
            printf("%s\n",INVALID_PORT );
            return -1;
        }
    }
    else
        port = 6423;
    
    numOfUsers = getNumOfUsers(users);
    fseek(users, 0, SEEK_SET);
    setup(database, users , database_unsorted);

    
    //init
    for (i = 0; i < NUM_OF_CLIENTS; i++)
    {
        client_socket[i] = 0;
        database[i].current_state = DISCONNECTED;
        database_unsorted[i].current_state = DISCONNECTED;

    }
    
    
    /*******************************************************************************************************************/
    /*******************************************************************************************************************/
    /***************************************************OPEN A SOCKET***************************************************/
    /*******************************************************************************************************************/
    /*******************************************************************************************************************/
    
    //setting up my sockaddr_in struct
    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(port);
    my_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    
    fd_listen_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (fd_listen_socket == -1){
        printf(FAILED_OPEN_SOCKET, strerror(errno));
        return -1;
    }
    
    if (bind(fd_listen_socket, (struct sockaddr*) (&my_addr),  sizeof(my_addr) ) < 0){
        printf(FAILED_BIND, strerror(errno));
        return -1;
    }
    
    inbox = (Mail*) malloc(MAXMAILS*sizeof(Mail));
    if (!inbox){
        printf(FAILED_BIND, strerror(errno));
        return -1;
    }
    if (listen(fd_listen_socket, NUM_OF_CLIENTS) == -1){
        printf(FAILED_LISTEN, strerror(errno));
        free(inbox);
        return -1;
    }
    
    while(1){
        /*****************************************SOCKET SET**********************************************************/
        FD_ZERO(&readSet);
        FD_SET(fd_listen_socket, &readSet);
        max_sd = fd_listen_socket;
        
        //add child sockets to set
        for ( i=0 ; i < NUM_OF_CLIENTS ; i++)
        {
            sd = client_socket[i];
            
            //if valid socket descriptor then add to read list
            if(sd > 0)
                FD_SET(sd,&readSet);
            
            //highest file descriptor number, need it for the select function
            if(sd > max_sd)
                max_sd = sd;
        }
        //wait for an activity on one of the sockets , timeout is NULL , so wait indefinitely
        if (select( max_sd + 1 , &readSet , NULL , NULL , NULL) <=0)
        {//no timout so cannot be equal to zero
            printf(FAILED_SELECT, strerror(errno));
        }
        
        //If something happened on the listening socket , then its an incoming connection
        if (FD_ISSET(fd_listen_socket, &readSet))
        {
            addr_size = sizeof(their_addr);
            fd_socket  = accept(fd_listen_socket, (struct sockaddr*) (&their_addr), &addr_size); //fd_socket is the new FD
            if (fd_socket == -1 ){
                printf(FAILED_ACCEPT, strerror(errno));
                free(inbox);
                return -1;
            }
            //add new socket to array of sockets
            for (i = 0; i < NUM_OF_CLIENTS; i++)
            {
                //if this is a new  client socket
                if( client_socket[i] == 0 )
                {
                    client_socket[i] = fd_socket;
                    //send a WELCOME message
                    if (send_message(fd_socket,WELCOME_MSG, strlen(WELCOME_MSG)) == -1){
                        printf(FAILED_SEND, strerror(errno));
                        free(inbox);
                        return -1;
                    }
                    break;
                }
            }
            continue;
        }
        for (i = 0; i < NUM_OF_CLIENTS; i++)
        {
            sd = client_socket[i];
            if (FD_ISSET( sd , &readSet))
            {
                //pass to FSM
                user_index = getUserIndexFromSocket(sd, database, NUM_OF_CLIENTS);
                if (user_index == -1)
                {
                    currentUser.current_state = ACCEPTED;
                    currentUser.socket_fd = sd;
                }
                else //connected user
                {
                    currentUser= database[user_index];
                }
                status = handleClient(&currentUser);
                if (status == -1 || currentUser.current_state == DISCONNECTED )
                {//user disconnected
                    
                    //Close the socket and mark as 0 in list for reuse
                    if (close(sd) == -1)
                    {
                        printf(FAILED_CLOSE, strerror(errno));
                    }
                    client_socket[i] = 0;
                    database[currentUser.user_index].socket_fd = -1;
                }
            }
        }
        
    }
}
