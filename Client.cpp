#include "Client.h"

TCPClient::TCPClient(const std::string& hostname, u_short port)
: socket_fd(-1),port_no(port),address(hostname)
{
	server.sin_family = AF_INET;
	server.sin_port = htons(port_no);
	server.sin_addr.s_addr = htonl(INADDR_ANY);
}

bool TCPClient::connectToServer()
{
	if(socket_fd == -1)
		socket_fd = socket(AF_INET, SOCK_STREAM, 0);
	if(socket_fd == -1)
	{
		PRT_ERR("Failed to create socket!")
		return false;
	}

	if(inet_addr(address.c_str()) == NO_IA)
	{
		struct hostent* he;
		struct in_addr** addr_list;

		if((he = gethostbyname(address.c_str())) == NULL)
		{
			PRT_ERR("Failed to resolve hostname.")
			return false;
		}

		addr_list = (struct in_addr **) he->h_addr_list;
		for(int i = 0; addr_list[i] != NULL; i++)
		{
			server.sin_addr = *addr_list[i];
			break;
		}
	}

	else server.sin_addr.s_addr = inet_addr(address.c_str());

	if (connect(socket_fd, (struct sockaddr *) &server, sizeof(server)) < 0)
	{
		PRT_ERR("Connection failed.")
		return false;
	}

    char welcome_buffer[MAX_BUFFER_SIZE];
	if(!recvBuff(socket_fd, welcome_buffer))
	{
		PRT_ERR("Receive failed.")
		return false;
	}
	
	// Print welcome message
	PRT_SUCC(std::string(welcome_buffer))
	return authenticate();
}

bool TCPClient::authenticate()
{
	
	char auth_buffer;
	std::string username, password;
	
	getline(std::cin, username, '\n');
	getline(std::cin, password, '\n');

	try
	{
		username = username.substr(6);
		password = password.substr(10);
	} catch (std::out_of_range& e) {
		PRT_ERR("Wrong syntax used for credentials. Exiting!")
	}

	std::string auth_data = username + " " + password;
	if(!sendBuff(socket_fd, auth_data))
	{
		PRT_ERR("Send failed.")
		return false;
	}

	if(recv(socket_fd, &auth_buffer, sizeof(char), 0) < 0)
	{
		PRT_ERR("Receive failed.")
		return false;
	}

	if(auth_buffer != 1)
	{
	    PRT_ERR("Authentication failed!")
		return false;
	}

	PRT_SUCC("Connected to server")
	return true;
}

void TCPClient::sendMsg(Message msg)
{
	if(!sendBuff(socket_fd, getCmdBuff(COMPOSE, msg.numOfRecipients())))
	{
		PRT_ERR("Send failed. Message was not sent.")
		return;
	}

	// Recipients
	if(!sendBuff(socket_fd, msg.recipients))
	{
		PRT_ERR("Send failed. Message was not sent.")
		return;
	}

	// Subject
	if(!sendBuff(socket_fd, msg.subject))
	{
		PRT_ERR("Send failed. Message was not sent.")
		return;
	}

	// Content
	if(!sendBuff(socket_fd, msg.text))
	{
		PRT_ERR("Send failed. Message was not sent.")
		return;
	}

//	PRT_DEBUG("Mail sent!")
}

void TCPClient::showInbox()
{
	if(!sendBuff(socket_fd, getCmdBuff(SHOW_INBOX, 0)))
	{
		PRT_ERR("Send failed. Could not show inbox.")
		return;
	}

	char msgs_buff[MAX_BUFFER_SIZE];
	std::string msgs;

	do
	{
		if(!recvBuff(socket_fd, msgs_buff))
		{
			PRT_ERR("Could not get number of msgs in inbox.")
			return;
		}

		msgs = std::string(msgs_buff);
		if(IS_CHAT(msgs))
		{
			Chat new_chat;
			genChat(&new_chat, msgs);
			chatQ.push(new_chat);
		}
	}
	while(true);

	int n_msgs = STR2INT(msgs);

//	PRT_DEBUG(n_msgs << " message(s) in inbox.")

	for(int i = 0; i < n_msgs; i++)
	{
		char buffer[MAX_BUFFER_SIZE];
		if(!recvBuff(socket_fd, buffer))
		{
			PRT_ERR("Failed to receive message #" << (i+1))
			continue;
		}
		else
			std::cout << std::string(buffer) << std::endl;
	}
}

void TCPClient::getMail(int id)
{
	if(!sendBuff(socket_fd, getCmdBuff(GET_MAIL, id)))
	{
		PRT_ERR("Send failed. Could not get mail.")
		return;
	}

	char vid_buff[MAX_BUFFER_SIZE];
	std::string valid_id;

	do
	{
		if(!recvBuff(socket_fd, vid_buff))
		{
			PRT_ERR("Could not validate mail ID.")
			return;
		}

		valid_id = std::string(vid_buff);
		if(IS_CHAT(valid_id))
		{
			Chat new_chat;
			genChat(&new_chat, valid_id);
			chatQ.push(new_chat);
		}
	}
	while(true);

	int vid = STR2INT(valid_id);

	if(vid != 0)
	{
//		PRT_DEBUG("before recv")
		// From
		char from[MAX_BUFFER_SIZE];
		if(!recvBuff(socket_fd, from))
		{
			PRT_ERR("Receive failed. (from)")
			return;
		}
//		PRT_DEBUG("after recv")
		// To
		char to[MAX_BUFFER_SIZE];
		if(!recvBuff(socket_fd, to))
		{
			PRT_ERR("Receive failed. (to)")
			return;
		}

		// Subject
		char subject[MAX_BUFFER_SIZE];
		if(!recvBuff(socket_fd, subject))
		{
			PRT_ERR("Receive failed. (subject)")
			return;
		}

		// Text
		char text[MAX_BUFFER_SIZE];
		if(!recvBuff(socket_fd, text))
		{
			PRT_ERR("Receive failed. (text)")
			return;
		}

		std::cout << std::string(from) << std::endl;
		std::cout << std::string(to) << std::endl;
		std::cout << std::string(subject) << std::endl;
		std::cout << std::string(text) << std::endl;
		return;
	}

	PRT_ERR("Invalid mail ID.")
	return;
}

void TCPClient::deleteMail(int id)
{
	if(!sendBuff(socket_fd, getCmdBuff(DELETE_MAIL, id)))
	{
		PRT_ERR("Send failed. Could not delete mail.")
		return;
	}
}

void TCPClient::quit()
{
	if(!sendBuff(socket_fd, getCmdBuff(QUIT, 0)))
	{
		PRT_ERR("Send failed. Could not quit.")
		return;
	}

	char quittable[MAX_BUFFER_SIZE];
	if(!recvBuff(socket_fd, quittable))
	{
		PRT_ERR("Receive failed in quit().")
		return;
	}

	std::string q(quittable);

	close(socket_fd);
}

void TCPClient::compose()
{
	Message new_msg;
	std::string to, subject, text;

	getline(std::cin, to, '\n');
	getline(std::cin, subject, '\n');
	getline(std::cin, text, '\n');

	new_msg.recipients = to.substr(4);
	new_msg.subject = subject.substr(9);
	new_msg.text = text.substr(6);
	sendMsg(new_msg);
}

void TCPClient::showOnlineUsers()
{
	if(!sendBuff(socket_fd, getCmdBuff(SHOW_ONLINE_USERS, 0)))
	{
		PRT_ERR("Send failed")
		return;
	}

	char usersnum[MAX_BUFFER_SIZE];
	std::string users;
	do
	{
		if(!recvBuff(socket_fd, usersnum))
		{
			PRT_ERR("Could not get number of online users")
			return;
		}

		users = std::string(usersnum);
		if(IS_CHAT(users))
		{
			Chat new_chat;
			genChat(&new_chat, users);
			chatQ.push(new_chat);
		}
	}
	while(true);

	int n_users = STR2INT(users);

	std::cout << "Online users: ";

	for(int i = 0; i < n_users; i++)
	{
		char buffer[MAX_BUFFER_SIZE];
		if(!recvBuff(socket_fd, buffer))
		{
			PRT_ERR("Failed to receive user #" << (i+1))
			continue;
		}
		else
			std::cout << std::string(buffer);

		if(i < n_users - 1)
			std::cout << ", ";
	}

	std::cout << std::endl;
}

void TCPClient::chat(const std::string& msg)
{
	std::string recipient, text;
	try
	{
		recipient = msg.substr(0, msg.find(':'));
		text = msg.substr(2 + msg.find(':'));
	} catch (std::out_of_range& e) {
		PRT_ERR("Wrong syntax used for chat. Correct syntax should be: MSG <user>: <message>")
		return;
	}

	if(!sendBuff(socket_fd, getCmdBuff(MSG, 0)))
	{
		PRT_ERR("Failed to send chat message.")
		return;
	}

	if(!sendBuff(socket_fd, recipient))
	{
		PRT_ERR("Failed to send chat message.")
		return;
	}

	if(!sendBuff(socket_fd, text))
	{
		PRT_ERR("Failed to send chat message.")
		return;
	}

//	PRT_DEBUG("Chat sent!")
}

void TCPClient::listenToChat()
{
	while(!chatQ.empty())
	{
		Chat c = chatQ.front();
		PRT_CHAT(c)
		chatQ.pop();
	}

	fd_set s;
	do
	{
		FD_RESET
		if(select(socket_fd+1, &s, NULL, NULL, NULL) < 0)
		{
			PRT_ERR("Select failed.")
			return;
		}

		if(FD_ISSET(0, &s)) return;

		if(FD_ISSET(socket_fd, &s))
		{
			char chat_buff[MAX_BUFFER_SIZE];
			if(!recvBuff(socket_fd, chat_buff))
			{
				PRT_ERR("Receive failed.")
				return;
			}

			std::string incoming(chat_buff);
			if(IS_CHAT(incoming))
			{
				Chat new_chat;
				genChat(&new_chat, incoming);
				PRT_CHAT(new_chat)
			}
		}
	}
	while(true);
}

TCPClient* initFromCLA(int argc, char* argv[])
{
	DEF_CLA
	
	if(argc == 1) return new TCPClient(hostname, port_no);

	if(argc == 3)
	{
		hostname = argv[1];
		long p = strtol(argv[2], NULL, 10);

		if(p < 0 || p > 65535)
		{
			PRT_ERR("Invalid port number!")
			return NULL;
		}
		port_no = (u_short) p;

		return new TCPClient(hostname, port_no);
	}

	PRT_ERR("Invalid number of arguments. Call: mail_client [hostname [port]]")
	return NULL;
}


int main(int argc, char* argv[])
{
	TCPClient* client;
	if((client = initFromCLA(argc, argv)) == NULL) return 1;
	if(!client->connectToServer()) return 1;

	fd_set set;
	std::string command;
	int int_param;
	std::string str_param;
	INIT_PARAM_COMM
	do
	{
//		PRT_DEBUG("Waiting for command...")
		client->listenToChat();

//		PRT_DEBUG("Back to main")
		getline(std::cin, command, '\n');
		std::transform(command.begin(), command.end(), command.begin(), ::tolower);

		EXECUTE("show_inbox", showInbox)
		EXECUTE("compose", compose)
		EXECUTE("show_online_users", showOnlineUsers)

		EXECUTE_INT_PARAM(get_mail, getMail)
		EXECUTE_INT_PARAM(delete_mail, deleteMail)

		EXECUTE_STR_PARAM(msg, chat)

		if(command != "quit") PRT_ERR("Invalid command, please try again.")
	}
	while(command != "quit");

	client->quit();
	delete(client);

	return 0;
}
